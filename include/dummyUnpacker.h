#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include "streamer.h"
#include "header.h"
#include "payload.h"
#include "condData.h"
#include "TFile.h"
#include "TTree.h"
#include <vector>

class dummyUnpacker{
public:
    dummyUnpacker(std::string fName_="", std::string outputFile_ = "dummy.root");
    ~dummyUnpacker();
    void loadFile(std::string fName);
    void printWord();
    void printBits();
    void getWord();
    void printEvent();
    void printBuffer();
    bool getEvent();
    void saveEvent();
    void writeFile();
    
private:
    streamer * ss;
    header  h_;
    payload p_;
    condData c_;
    TFile * output_;
    TTree * t2;
    
    // FLAT NTUPLE variables
    std::vector <int>       hit0;
    std::vector <int>       hit1;
    std::vector <int>       stub;
    std::vector <int>       bendCode;
    std::vector <float>     bend;
    std::vector <int>       stubSim;
    std::vector <int>       bendCodeSim;
    std::vector <int>       bendSim;

    std::vector <uint16_t>  CbcL1A;
    std::vector <uint16_t>  Pipeline;
    std::vector <uint16_t>  Error;

    uint32_t           L1A;
    uint8_t            TDC;
    uint32_t           Timestamp;
    uint16_t           delay_and_group;
    uint16_t	       vcth;
};
