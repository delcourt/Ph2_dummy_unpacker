#include "../include/streamer.h"
#include <assert.h>
using namespace std;

uint64_t streamer::getSubWord(int start, int stop){
    if (start < stop){
        printEvent();
        cout<<start<<"-"<<stop<<endl;
    }
    assert (start >= stop);
    assert (start < 64);
    assert (stop > -1);
//     cout<<"***"<<endl;
//     cout<<dec<<start<<"-"<<stop<<endl;
//     cout<<hex<<(word_>>stop)<<endl;
//     cout<<(((uint64_t)1)<<(start-stop+1))<<endl;
//     cout<<((word_>>stop)%(((uint64_t)1)<<(start-stop+1)))<<endl;
//     cout<<"***"<<endl;
    if (start-stop+1 == 64)
        return word_;
    return( ((word_>>stop)%(((uint64_t)1)<<(start-stop+1))) );
}


streamer::streamer(string fName_){
    event_ = new uint64_t[5000];
    storeEvent_=true;
    inputFile_.open(fName_.c_str(),ios::in | ios::binary);
    if (!inputFile_.is_open())
      cerr<<"Error, file not opened correctly !"<<endl;
    inputFile_.seekg (0, inputFile_.end);
    fileSize_ = inputFile_.tellg();
    inputFile_.seekg (0, inputFile_.beg);
}

void streamer::printWord(){
    cout<<wordCounter_<<"| ";
     for (int i = 0 ; i < 16; i++){
         cout<<hex<<int((word_>>(60-i*4))%16);
         if (i%4==3)
             cout<<" ";
     }
     cout<<endl;
}

void streamer::printEvent(){
    cout<<"Printing event..."<<endl;
    uint64_t wordBak = word_;
    cout<<"Word counter :"<<wordCounter_<<endl;
    for (int i = 0; i < wordCounter_; i++){
        word_ = event_[i];
        printWord();
    }
    word_ = wordBak;
}

void streamer::printBits(){
    for (int i = 63; i> 0; i--)
        cout<<(word_>>i)%2;
     cout<<endl;
}
void streamer::reorder(){
    uint64_t word_shuffle(word_);
    word_ = 0;
    uint64_t one = 1;
    uint64_t firstWord  = (word_shuffle>>32);
    uint64_t secondWord = (word_shuffle)%(one<<32);
    
    for (int i = 0; i < 4; i++){
        word_ |= ((firstWord >> 8*i) % 256) << ( 56 - 8*i);
        word_ |= ((secondWord >> 8*i) % 256) << ( 24 - 8*i);
    }
}

void streamer::getWord(){
    inputFile_.read(buffer,8);
    word_=0;
    for (int i = 0; i< 8; i++){
        word_|=(uint64_t(uint8_t(buffer[i])))<<(56-8*i);
    }
    reorder();
    
    if (storeEvent_){
        event_[wordCounter_] = word_;
        wordCounter_++;
        if (wordCounter_ > 3000){
            cerr<<"Error, exceeded event buffer (1024 words deep)"<<endl;
	    printEvent();
            storeEvent_ = false;
        }
    }
//    this->printWord();
}

bool streamer::endEvent(){
    getWord();
//     printEvent();
    if (storeEvent_){
//         cout<<"Event size = "<<getSubWord(55,32)<<endl;
        //cout<<wordCounter_<<endl;
        //cout<<getSubWord(55,32)<<endl;
        //if (wordCounter_-getSubWord(55,32) != 1)
        //    cout<<"Difference is not 1 !!!" << wordCounter_-getSubWord(55,32) <<endl;
        assert (getSubWord(55,32) == wordCounter_);
        // Skipping assertion because of off-by-one offset not understood...
    }
    // WARNING, not checking CRC
    //this->printEvent();
    wordCounter_ = 0;
//    cout<<inputFile_.tellg()-fileSize_<<endl;
//    cout<<inputFile_.eof()<<endl;
    return(inputFile_.tellg() != fileSize_);
}
