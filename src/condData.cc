 
#include "../include/condData.h"
#include <assert.h>
#include <iostream>

using namespace std;

void condData::Fill(streamer * ss){
    reset();
    ss->getWord();
    //ss->printWord();
    // First word counts condition data.
    nCond = ss->word_;
    
    for (uint64_t i = 0; i < nCond; i++){
        ss->getWord();
        //ss->printWord();
        cond[ss->getSubWord(63,32)] = ss->getSubWord(31,0);
    }
}

void condData::reset(){
    nCond = 0;
    cond.clear();
}

void condData::print(){
    for (auto data : cond)
        cout<<hex<<data.first<<" --> "<<data.second<<endl;
}
