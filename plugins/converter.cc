#include "include/dummyUnpacker.h"
#include <assert.h>
#include <iostream>
#include "TRandom3.h"
using namespace std;

const int prescale = 1;


int main(int argc, char ** argv){
    assert(argc == 3);
    dummyUnpacker d(argv[1],argv[2]);
    TRandom3 * r = new TRandom3;
    int evtCounter = 0;
    while (d.getEvent()){
        if (evtCounter%10000 == 0){
            cout<<evtCounter<<endl;
	}
	if (r->Rndm() < 1./prescale){
	    d.saveEvent();	
	}
        evtCounter++;
    }
    cout<<"Saving event..."<<endl;
    d.saveEvent();
    cout<<"Writing to file..."<<endl;
    d.writeFile();
}
